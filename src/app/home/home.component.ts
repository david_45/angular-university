import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import {Students} from '../students';
import {updateStudents} from '../updatestudents';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HttpService]
})
export class HomeComponent implements OnInit {
  FacultyName = '';
  FS = false;
  FSerror = false;
  Faculty = [];


  CourseName = '';
  CS = false;
  CSerror = false;
  Course = [];

  GroupName = '';
  GS = false;
  GSerror = false;
  Group = [];

  Students = [];
  deleteStudent = false;
  UpdateInfo = [];
  GrName = '';
  CoName = '';
  FaName = '';
  AllSrtudents = [];
  searchStr = '';

  FilterStudents = [];
  AllCourse = [];
  AllGroup = [];
  AllFaculty = [];

  Facultyselected = false;
  Courseselected = false;
  FormVald = false;
  StudentCreateMessage = false;
  StudentUpdateMessage = false;


  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
    this.httpService.getFacultyData().subscribe(
      (data) => {
        this.Faculty = data.faculty;
        this.AllFaculty = data.faculty;
      });

    this.httpService.getCoursedata().subscribe(
      (data) => {
        this.Course = data.course;
        this.AllCourse = data.course;

      });
    this.httpService.getGroupdata().subscribe(
      (data) => {
        this.Group = data.group;
        this.AllGroup = data.group;
      });

    this.httpService.getStudentsdata().subscribe(
      (data) => {
        this.Students = data.students;
        this.AllSrtudents = data.students;
      });
  }

  CloseUp() {
    this.UpdateInfo = [];
    this.FS = false;
    this.FSerror = false;
    this.CS = false;
    this.CSerror = false;
    this.GS = false;
    this.GSerror = false;

  }

  FacultySubmit() {

    if (this.FacultyName !== '') {
      this.httpService.FacultyData(this.FacultyName)
        .subscribe((data) => {
          this.Faculty.push(data.faculty);
          this.FacultyName = '';
          this.FS = true;
          this.FSerror = false;
        });

    } else {
      this.FSerror = true;
    }
  }

  CourseSubmit(id) {

    if (this.CourseName !== '') {
      this.httpService.CourseData(this.CourseName, id)
        .subscribe((data) => {
          this.CourseName = '';
          this.CS = true;
          this.CSerror = false;
          this.ngOnInit();

        });
    } else {
      this.CSerror = true;
    }
  }


  GroupSubmit(id) {

    if (this.GroupName !== '') {
      this.httpService.GroupData(this.GroupName, id)
        .subscribe((data) => {
          this.GroupName = '';
          this.GS = true;
          this.GSerror = false;
          this.ngOnInit();

        });
    } else {
      this.GSerror = true;
    }

  }

  StudentsSubmit(studentsData: Students) {
    console.log(studentsData);
    this.httpService.StudentsData(studentsData)
      .subscribe((data) => {
        this.Students.push(data.students);
        this.StudentCreateMessage =  true;
      });
  }


  DeleteSt(students, index) {
    this.Students.splice(index, 1);
    this.httpService.deleteStudentData(students.id)
      .subscribe((data) => {
        this.deleteStudent = true;
      });
  }

  UpdateStudent(student) {

    for (const grup of this.Group) {
      if (grup.id === student.group_id) {
        this.GrName = grup.name;
      }
    }
    for (const course of this.Course) {
      if (course.id === student.course_id) {
        this.CoName = course.name;
      }
    }

    for (const fac of this.Faculty) {
      if (fac.id === student.faculties_id) {
        this.FaName = fac.name;
      }
    }

    this.UpdateInfo.push(student);
  }

  UpdateStudentData(student: updateStudents) {
    this.httpService.UpdateStudentData(student)
      .subscribe((data) => {
        this.Students = data.students;
        this.StudentUpdateMessage = true;
      });
  }


  SelectFacFilterSearch(index) {
    this.Students = this.AllSrtudents;
    const y = +index;
    for (const student of this.Students) {
      if (student.faculties_id === y) {
        this.FilterStudents.push(student);
      }
    }
    this.Facultyselected = true;
    this.Students = [];
    this.Students = this.FilterStudents;
    this.FilterStudents = [];
  }

  SelectCouFilterSearch(index) {
    this.Students = this.AllSrtudents;
    const y = +index;
    for (const student of this.Students) {
      if (student.course_id === y) {
        this.FilterStudents.push(student);
      }
    }
    this.Courseselected = true;
    this.Students = [];
    this.Students = this.FilterStudents;
    this.FilterStudents = [];
  }

  SelectGroupFilterSearch(index) {
    this.Students = this.AllSrtudents;
    const y = +index;
    for (const student of this.Students) {
      if (student.group_id === y) {
        this.FilterStudents.push(student);
      }
    }
    this.Students = [];
    this.Students = this.FilterStudents;
    this.FilterStudents = [];
  }


  SelectFacfilter(index) {
    this.Course = this.AllCourse;
    const Array = [];
    const y = +index;
    for (const cours of this.Course) {
      if (cours.faculty_id === y) {
        Array.push(cours);
        this.Facultyselected = true;
      }
    }
    this.Course = Array;

  }

  SelectCoufilter(index) {
    this.Group = this.AllGroup;
    const Array = [];
    const y = +index;
    for (const gru of this.Group) {
      if (gru.course_id === y) {
        Array.push(gru);
        this.Courseselected = true;
      }
    }
    this.Group = Array;

  }

  Refresh(form: NgForm) {
    this.Group = this.AllGroup;
    this.Course = this.AllCourse;
    this.Faculty = this.AllFaculty;
    this.Students = this.AllSrtudents;
    this.Facultyselected = false;
    this.Courseselected = false;
    this.FormVald = false;
    this.CloseUp();
    form.reset();
    this.StudentCreateMessage = false;
    this.StudentUpdateMessage = false;
  }

  SelectGroupFilterSearchSub(index) {
    this.FormVald = true;
  }

  RefreshAllSelect() {
    this.Group = this.AllGroup;
    this.Course = this.AllCourse;
    this.Faculty = this.AllFaculty;
    this.Students = this.AllSrtudents;
    this.CloseUp();
  }


}
