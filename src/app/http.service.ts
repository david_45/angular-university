import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Students} from './students';
import {updateStudents} from './updatestudents';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


@Injectable()
export class HttpService {

  constructor(private http: Http) {
  }


  FacultyData(faculty) {

    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

    return this.http.post('http://laravel-angular.dev/api/postfaculty', {name: faculty}, {headers: headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      })
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

  getFacultyData() {
    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});
    return this.http.get('http://laravel-angular.dev/api/getfaculty').map(
      (response: Response) => {
        const data = response.json();
        return data;
      });
  }


  CourseData(name, faculty_id) {

    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

    return this.http.post('http://laravel-angular.dev/api/postcourse', {
      name: name,
      faculty_id: faculty_id
    }, {headers: headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      })
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

  getCoursedata() {
    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});
    return this.http.get('http://laravel-angular.dev/api/getcourse').map(
      (response: Response) => {
        const data = response.json();
        return data;
      });
  }

  GroupData(name, course_id) {

    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

    return this.http.post('http://laravel-angular.dev/api/postgroup', {
      name: name,
      course_id: course_id
    }, {headers: headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      })
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

  getGroupdata() {
    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});
    return this.http.get('http://laravel-angular.dev/api/getgroup').map(
      (response: Response) => {
        const data = response.json();
        return data;
      });
  }

  StudentsData(students: Students) {

    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

    return this.http.post('http://laravel-angular.dev/api/poststudents', students)
      .map(
        (response: Response) => {
          const data = response.json();
          return data;
        })
      .catch((errorRes: any) => {
        return Observable.throw(errorRes);
      });
  }

  getStudentsdata() {
    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});
    return this.http.get('http://laravel-angular.dev/api/getstudents').map(
      (response: Response) => {
        const data = response.json();
        return data;
      });
  }

  deleteStudentData(student_id) {

    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

    return this.http.post('http://laravel-angular.dev/api/deletestudents', {
      id: student_id
    }, {headers: headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      })
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }

  UpdateStudentData(students: updateStudents) {

    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

    return this.http.post('http://laravel-angular.dev/api/updatetudents', students)
      .map(
        (response: Response) => {
          const data = response.json();
          return data;
        })
      .catch((errorRes: any) => {
        return Observable.throw(errorRes);
      });
  }


  SelectStudentCourse(course_id) {

    const headers = new Headers({'Content-Type': 'application/json;charset=utf-8'});

    return this.http.post('http://laravel-angular.dev/api/selectcourse', {
      id: course_id
    }, {headers: headers})
      .map((resp: Response) => {
        const data = resp.json();
        return data;
      })
      .catch((error: any) => {
        return Observable.throw(error);
      });
  }
}

