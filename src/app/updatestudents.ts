export class updateStudents {
  id?: number;
  name?: string;
  surname?: string;
  patronymic?: string;
  phone?: string;
  address?: string;
  group_id?: number;
  course_id?: number;
  faculties_id?: number;
}
