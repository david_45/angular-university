import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'search',
  pure: false
})
export class SearchPipe implements PipeTransform {
  transform(students, value) {
    return students.filter(student => {
      return student.name.includes(value);
    });
  }
}
