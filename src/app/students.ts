export class Students {
  name?: string;
  surname?: string;
  patronymic?: string;
  phone?: string;
  address?: string;
  group_id?: number;
  course_id?: number;
  faculties_id?: number;
}
